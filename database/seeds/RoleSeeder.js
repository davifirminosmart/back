'use strict'

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class RoleSeeder {
  async run () {
    let role = await Factory.model('Adonis/Acl/Role').createMany(2)
    const smartRole = role[0]
    const suporteRole = role[1]
    
    // const consultivoRole = role[3]
    // const basicoRole = role[4]
    // const avancadoRole = role[5]
    

    // consultivoRole.permissions().attach([4, 5, 6,10, 12, 16])
    smartRole.permissions().attach([1, 2, 3, 4])
    suporteRole.permissions().attach([4,5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ,20, 21])
    // basicoRole.permissions().attach([4,5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17])
    // avancadoRole.permissions().attach([4,5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21])
  }
}

module.exports = RoleSeeder
