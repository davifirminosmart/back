'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('Adonis/Acl/Role', async (faker, i) => {
    return {
      name: ['Smart', 'Suporte'][i],
      id: [1, 2][i],
      slug: ['smart', 'suporte'][i],
      description: ['Usuário smart', 'Usuário Suporte'][i]
    }
    // return {
    //     name: ['Smart', 'Admin', 'Suporte Smart', 'Consultivo', 'Basico', 'Avancado'][i],
    //     id: [1, 2, 3, 4, 5, 6][i],
    //     slug: ['smart', 'admin', 'suporte', 'consultivo', 'basico', 'avancado'][i],
    //     description: ['Usuário smart', 'Usuário admin', 'Usuário suporte', 'Consultivo', 'Basico', 'Avançado'][i]
    // }
})

Factory.blueprint('Adonis/Acl/Permission', async (faker, i) => {
  return {
      name: ['Criar Empresa', 'Editar Empresa', 'Deletar Empresa', 'Visualizar TimeLine', 'Visualizar Profile', 'Editar Profile', 'Visualizar Cartao', 'Bloquear Cartao', 'Segunda Via Cartao', 'Trocar Senha Cartao','Historico Cartao', 'Adicionar Cartao', 'Detalhe Fatura', 'Enviar SMS', 'Enviar E-mail', 'Detalhe Cartao', 'Desbloquear Cartão', 'Alterar Limite', 'Contestação', 'Solicitação Especial', 'Senha SMS'][i],
      id: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21][i],
      slug: ['create_company', 'edit_company', 'delete_company', 'view_timeline', 'view_profile', 'edit_profile', 'view_card', 'block_card', 'second_via_card', 'password_card', 'history_card', 'add_card', 'detailed_invoice', 'send_sms', 'send_email', 'view_detail_card', 'unblock_card', 'change_limit', 'contestation', 'special_request', 'card_sms'][i],
      description: ['Empresa', 'Empresa', 'Empresa', 'TimeLine', 'Perfil', 'Perfil', 'Cartão', 'Cartão', 'Cartão', 'Cartão', 'Cartão', 'Cartão', 'Fatura', 'Fatura', 'Fatura', 'Cartão', 'Cartão', 'Cartão', 'Cartão', 'Cartão', 'Cartão'][i]
  }
})

Factory.blueprint('App/Models/User', (faker, i) => {
  return {
    username: ['Admin Smart'][i],
    email: ['admin@smart.com'][i],
    birthday:['12/12/2012'][i],
    password: ['102030'][i]
  }
})
