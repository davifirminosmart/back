'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanyUserSchema extends Schema {
  up () {
    this.create('company_user', (table) => {
      table.increments()
      table.integer('user_id').unsigned().index()
      table.integer('company_id').unsigned().index()

      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.foreign('company_id').references('id').on('companies').onDelete('cascade')
    })
  }

  down () {
    this.drop('company_user')
  }
}

module.exports = CompanyUserSchema
