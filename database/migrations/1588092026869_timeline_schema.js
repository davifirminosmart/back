'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TimelineSchema extends Schema {
  up () {
    this.create('timelines', (table) => {
      table.increments()
      table.string('protocol', 90)
      table.integer('user_id').unsigned().index()
      table.integer('id_conductor')
      table.string('client_cpf',11)
      table.string('title', 100)
      table.text('description')
      table.integer('company_id').unsigned().index()
      table.timestamps()
      
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.foreign('company_id').references('id').on('companies').onDelete('cascade')
    })
  }

  down () {
    this.drop('timelines')
  }
}

module.exports = TimelineSchema
