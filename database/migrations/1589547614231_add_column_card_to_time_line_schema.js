'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnCardToTimeLineSchema extends Schema {
  up () {
    this.raw("ALTER TABLE timelines ADD COLUMN card enum('1', '0') DEFAULT '0';")
  }

  down () {
  }
}

module.exports = AddColumnCardToTimeLineSchema
