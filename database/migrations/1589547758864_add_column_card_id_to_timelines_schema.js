'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnCardIdToTimelinesSchema extends Schema {
  up () {
    this.raw("ALTER TABLE timelines ADD COLUMN card_id INT(11) DEFAULT NULL;")
  }

  down () {
  }
}

module.exports = AddColumnCardIdToTimelinesSchema
