'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.create('companies', (table) => {
      table.string('name', 90)
      table.string('cnpj', 30)
      table.integer('help-desk').defaultTo(0)
      table.string('tokenHelpDesk', 250)
      table.string('userConductor', 60)
      table.string('passwordConductor', 60)
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('companies')
  }
}

module.exports = CompanySchema
