'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TicketSchema extends Schema {
  up () {
    this.create('tickets', (table) => {
      table.increments()
      table.string('protocol', 90)
      table.string('client_cpf', 20)
      table.integer('user_id').unsigned().index()
      table.integer('id_conductor')
      table.integer('company_id').unsigned().index()
      table.timestamps()

      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.foreign('company_id').references('id').on('companies').onDelete('cascade')
    })
  }

  down () {
    this.drop('tickets')
  }
}

module.exports = TicketSchema
