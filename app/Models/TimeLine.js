'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TimeLine extends Model {
  static get table() {
    return 'timelines'
  }
}

module.exports = TimeLine
