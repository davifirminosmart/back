'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Model = use('Model')
const Env = use('Env')
const request = require("request");
const API_URL = Env.get('CONDUCTOR_URL')
const API_TOKEN = Env.get('CONDUCTOR_TOKEN')
const API_URL_NOVA = 'https://api-v3-bandeira.marketpay.com.br/api/api/v3/'
const NOVO_TOKEN = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydG54LWJlY2tlciIsImNyZWF0ZWQiOjE1OTQwNjE4OTQ0NTcsInJvbGVzIjpbIlJPTEVfQ0FSRFNfQVBJLkFOVUlEQURFX1BST0RVVE8uRVNDUklUQSIsIlJPTEVfQ0FSRFNfQVBJLkFOVUlEQURFX1BST0RVVE8uTEVJVFVSQSIsIlJPTEVfQ0FSRFNfQVBJLkJBSVhBX0JBTkNBUklBLkVTQ1JJVEEiLCJST0xFX0NBUkRTX0FQSS5DQVBBTkEuRVNDUklUQSIsIlJPTEVfQ0FSRFNfQVBJLkNBUEFOQS5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuQ0FSVEFPLkVTQ1JJVEEiLCJST0xFX0NBUkRTX0FQSS5DQVJUQU8uTEVJVFVSQSIsIlJPTEVfQ0FSRFNfQVBJLkNJQ0xPLkxFSVRVUkEiLCJST0xFX0NBUkRTX0FQSS5DTElFTlRFX0pVUklESUNPLkVTQ1JJVEEiLCJST0xFX0NBUkRTX0FQSS5DT05UQV9CQU5DQVJJQS5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuRU1JU1NPUi5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuRVNDT0xBUklEQURFLkxFSVRVUkEiLCJST0xFX0NBUkRTX0FQSS5MQU5DQU1FTlRPX0FKVVNURS5FU0NSSVRBIiwiUk9MRV9DQVJEU19BUEkuTE9KQS5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuTE9URS5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuTU9SQURJQS5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuT0NVUEFDQU8uTEVJVFVSQSIsIlJPTEVfQ0FSRFNfQVBJLlBBUkFNRVRSTy5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuUE9SVEFET1IuRVNDUklUQSIsIlJPTEVfQ0FSRFNfQVBJLlBPUlRBRE9SLkxFSVRVUkEiLCJST0xFX0NBUkRTX0FQSS5QUk9EVVRPLkxFSVRVUkEiLCJST0xFX0NBUkRTX0FQSS5QUk9GSVNTQU8uTEVJVFVSQSIsIlJPTEVfQ0FSRFNfQVBJLlJFR0lTVFJPX0NPQlJBTkNBLkVTQ1JJVEEiLCJST0xFX0NBUkRTX0FQSS5TRUdNRU5UTy5MRUlUVVJBIiwiUk9MRV9DQVJEU19BUEkuU0VHVVJPLkVTQ1JJVEEiLCJST0xFX0NBUkRTX0FQSS5USVBPX0xBTkNBTUVOVE8uTEVJVFVSQSJdLCJpZCI6MjgsImV4cCI6MTU5NDQ5Mzg5NH0.2_UJbqC9xUdCf2ib-Bhgm3RNdyFHtjzcDmieTz_HFrrmsM_0WitFeSx1KXKNq2O8nRPw5KxE28eCik0DmLsMow'
const Company = use('App/Models/Company')

class Conductor extends Model {

  static async getToken(companyId) {
    const company = await Company.find(companyId)
    var options = { method: 'POST',
    url: `https://api-v3-bandeira.marketpay.com.br/autenticacao/tokens`,
    headers: {
      "Content-Type": "application/json",
     },
    body: {
      login: company.userConductor,
      senha: company.passwordConductor
    },
    json: true };


    
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getClient(params, companyId) {

    let { data } = await this.getToken(companyId)
    params = this.serialize({...params})

    var options = { method: 'GET',
    url: `${API_URL}portadores/consulta-cliente-cpf?${params}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };
    console.log('getClient', options)
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }


  static async getCompany(params, companyId) {

    let { data } = await this.getToken(companyId)
    params = this.serialize({...params})

    var options = { method: 'GET',
    url: `${API_URL}portadores/consulta-cliente-cnpj?${params}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };
    console.log('getCompany', options)
    
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getAnuidade(id, companyId) {
    
    let { data } = await this.getToken(companyId)
    
    var options = { method: 'GET',
    url: `${API_URL}anuidades/consultar/${id}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getAdicional(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}/cartoes/adicional/${id}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async cancelarAnuidade(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'PATCH',
    url: `${API_URL}anuidades/${id}/anuidade/cancelamento`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async tiposBloqueio(companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}cartoes/tipos-bloqueio`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }


  static async blockCard(id, obs, tipo, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'PATCH',
    url: `${API_URL_NOVA}cartoes/${id}/bloqueio`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
     body: { 
      observacao : obs,
      idTipoBloqueio: tipo
    },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async desbloquear(id, observacao, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'PATCH',
    url: `${API_URL_NOVA}cartoes/${id}/desbloqueio`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
     body: { 
       observacao : observacao
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }
  
  static async updateClient(params, id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'PATCH',
    url: `${API_URL}portadores/${id}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
    body: params,
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getClientContact(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}portadores/${id}/telefones/`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async faturaFechada(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}portadores/${id}/fatura-fechada/`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async faturaAtual(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}portadores/${id}/fatura-atual/`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async informacoesCliente(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}portadores/${id}/informacoes/`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async consultarCartaoTitular(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL_NOVA}cartoes/titular/${id}/`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };
    
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getClientById(params, companyId) {
    let { data } = await this.getToken(companyId)
    params = this.serialize({...params})

    var options = { method: 'GET',
    url: `${API_URL}portadores/consulta-cliente-id?${params}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static serialize (obj) {
    var str = []
    for (var p in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
      }
    }
    return str.join('&')
  }

  static async getCardsAdicional(params) {
    
    let { data } = await this.getToken()
    params = this.serialize({...params})

    var options = { method: 'GET',
    url: `${API_URL}cartoes/consultar-por-dependente?${params}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async listInvoiceByPeriod(client_id, dt_ini, dt_end, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}portadores/${client_id}/fatura-periodo?data-final=${dt_end}&data-inicial=${dt_ini}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async adicional(params,id_titular, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'POST',
    url: `${API_URL_NOVA}cartoes/adicional/${id_titular}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
    body: params,
    json: true };
    console.log('HEEEEEAADERRRR')
    console.log(options)
    console.log('CONTATOS')
    console.log(params.telefones)
    console.log('Enderecos')
    console.log(params.endereco)
    
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async listInvoiceDetailed(client_id, invoice_id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}portadores/${client_id}/fatura-detalhada?id_fatura=${invoice_id}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getBankAccount(companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'GET',
    url: `${API_URL}contas-bancarias`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async getBarcodes(invoice_id, bank_id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'POST',
    url: `${API_URL}boletos/${invoice_id}/registrar?idContaBancaria=${bank_id}`,
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async smsCard(id, companyId) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'PATCH',
    url: `${API_URL_NOVA}cartoes/${id}/restaurar-senha`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async novoContato(params, companyId, idCliente) {
    let { data } = await this.getToken(companyId)
    var options = { method: 'POST',
    url: `${API_URL_NOVA}/portadores/${idCliente}/telefones/`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data}`,
     },
    body: {
      "faturaDigital": false,
      "receberSMS": false,
      "tipo": params.tipoContato,
      "area": params.ddd.replace('(', '').replace(')', ''),
      "telefone": params.numero.replace('-','')
    },
    json: true };
    
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }
}

module.exports = Conductor
