'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Ticket extends Model {
  static boot () {
    super.boot()

    this.addHook('beforeSave', async (ticketInstance) => {
      ticketInstance.protocol = new Date().getTime() +  1 + Math.floor((100 - 1) * Math.random())
    })
  }
}

module.exports = Ticket
