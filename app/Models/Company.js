'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Company extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeSave', async (companyInstance) => {
      companyInstance.cnpj = companyInstance.cnpj.split('.').join('').replace('-', '').replace('/', '')
      companyInstance['help-desk'] = companyInstance['help-desk'] ? 1 : 0
    })
  }

  users () {
    return this.belongsToMany('App/Models/User')
  }
}

module.exports = Company
