'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')
const INFOBIP_URL = Env.get('INFOBIP_URL')
const request = require("request");

class Infobip extends Model {

  static async sendSMS(telephone, message, base64) {
    var options = { method: 'POST',
    url: `${INFOBIP_URL}sms/2/text/advanced`,
    headers: {
      Authorization: `Basic ${base64}`,
    },
    body: {
      "messages": [
        {
          "destinations": [
            {
              "to": `55${telephone}`
            }
          ],
          "text": message
        }
      ]
    },
    json: true };

    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

}

module.exports = Infobip
