'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')
const request = require("request");
const API_URL = Env.get('HELPDESK_URL')
const API_TOKEN = Env.get('HELPDESK_TOKEN')
const Company = use('App/Models/Company')

class HelpDesk extends Model {

  static async token (companyId) {
    let company = await Company.find(companyId)
    return company.tokenHelpDesk
  }
  
  static serialize (obj) {
    var str = []
    for (var p in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + ' eq ' + `'${ encodeURIComponent(obj[p])}'` )
      }
    }
    return str.join('&')
  }

  static async getClient(params, companyId) {
    const token = await this.token(companyId)
    params = this.serialize(params)
    var options = { method: 'GET',
    url: `${API_URL}persons?token=${token}&$filter=${params}`,
    headers:
        { 'content-type': 'application/json' },
    json: true };
    console.log(options)
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }

  static async createClient(params, companyId) {
    const token = await this.token(companyId)
    var options = { method: 'POST',
    url: `${API_URL}persons?token=${token}`,
    headers:
        { 'content-type': 'application/json' },
    body: params,
    json: true };
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }
  
  static async createTicket(id, personType, description, subject, companyId) {
    const token = await this.token(companyId)
    var options = { method: 'POST',
    url: `${API_URL}/tickets?token=${token}`,
    headers:
        { 'content-type': 'application/json' },
    body:{ type: 1,
      subject,
      clients: [{ id: id, personType, profileType: 2 }], 
      createdBy: { id: id, personType, profileType: 1 },
      actions: [ {type: 1, description }]
    },
    json: true };
    console.log(options)
    return new Promise( (resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) reject(new Error(error))
            if(response) resolve(response.body)
        });
    })
  }
}

module.exports = HelpDesk
