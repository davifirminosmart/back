'use strict'
const Database = use('Database')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
      const birthday = userInstance.birthday.split('/')
      userInstance.birthday = `${birthday[2]}-${birthday[1]}-${birthday[0]}`
    })
  }

  static get traits () {
    return [
      '@provider:Adonis/Acl/HasRole',
      '@provider:Adonis/Acl/HasPermission'
    ]
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  company () {
    return this.belongsToMany('App/Models/Company')
  }

  async getNameRole(user_id) {
    try {
      const role = await Database
        .select('roles.slug')
        .table('roles')
        .innerJoin('role_user', 'role_user.role_id', 'roles.id')
        .innerJoin('users', 'role_user.user_id', 'users.id')
        .where('users.id', user_id) 
      return role[0].slug 
    } catch(error) {
      throw new Error(error.message)
    }
  }
}

module.exports = User
