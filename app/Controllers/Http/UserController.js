'use strict'
const User = use('App/Models/User')
class UserController {

  async create ({ request, response, auth}) {
    try {
      const { group } = request.all()
      const userAuth = await auth.getUser()
      let company = await userAuth.company().fetch()
      const data = request.only(["username", "email", "password", "cpf", "birthday"])
      const user = await User.create(data)
      await user.roles().attach([group])
      await user.company().attach([company.rows[0].id])
      return response.status(200).json({ dataUser: user })
    } catch (error) {
      return response.status(401).json({ message: 'Usuários não encontrados', original: error.message })
    }
  }

  async index ({response }) {
    try {
      const users = await User
      .query()
      .select('users.id','users.username', 'users.cpf', 'users.email', 'roles.name', 'users.birthday', 'roles.id as group')
      .innerJoin('role_user', 'users.id', 'role_user.user_id')
      .innerJoin('roles', 'role_user.role_id', 'roles.id')
      .whereNot('roles.id', 2)
      .whereNot('roles.id', 1)
      .fetch()
      response.status(200).json({ users })
    } catch (error) {
      console.log(error.message)
      return response.status(401).json({ message: 'Usuários não encontrados', original: error.message })
    }
  }

  async update ({ request, response }) {
    try {
      const { group, id, password } = request.all()
      let data = null
      if(password == '1234') data = request.only(["username", "email", "cpf", "birthday"])
      if(password != '1234') data = request.only(["username", "email", "password", "cpf", "birthday"])
      const user = await User.find(id)
      await user.merge(data)
      await user.save()
      await user.roles().detach()
      await user.roles().attach([group])
      return response.status(200).json({ dataUser: user })
    } catch (error) {
      return response.status(401).json({ message: 'Usuários não encontrados', original: error.message })
    }
  }
}

module.exports = UserController
