'use strict'

const Conductor = use('App/Models/Conductor')
const Moment = require('moment')

class InvoiceController {
  async list({ request, response, auth }) {
    try { 
      const { id, type } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      // const today = Moment().startOf('year').format('YYYY-MM-DD')
      const initial = '1993-02-15'
      const lastMonth = Moment().endOf('year').format('YYYY-MM-DD') 
      let resp = await Conductor.listInvoiceByPeriod(id, initial, lastMonth, company.id)
      let faturas = resp.faturasResponse.map(resp => {
        resp.dataVencimento = Moment(resp.dataVencimento).format('DD/MM/YYYY')
        return resp
      })
      // faturas = faturas.filter(fatura => {
      //   return ((fatura.status == type))
      // })
      return response.status(200).json(
        { 
          message: 'Listado com sucesso', 
          data: faturas
        }
      )
    } catch(error) {
      return response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async detailed({ request, response, auth }) {
    try { 
      const { client_id, invoice_id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      let resp = await Conductor.listInvoiceDetailed(client_id, invoice_id, company.id)
      resp.dataVencimento = Moment(resp.dataVencimento).format('DD/MM/YYYY')
      resp.lancamentos = resp.lancamentos.map(lancamento => {
        lancamento.dataLancamento = Moment(lancamento.dataLancamento).format('DD/MM/YYYY')
        lancamento.dataTransacao = Moment(lancamento.dataTransacao).format('DD/MM/YYYY')
        return lancamento
      })
      return response.status(200).json(
        {
          message: 'Listado com sucesso',
          data: resp
        }
      )
    } catch(error) {
      return response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
}

module.exports = InvoiceController
