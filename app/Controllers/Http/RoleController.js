'use strict'
const Role = use('Adonis/Acl/Role')
class RoleController {

    async list ({ auth, response }) {
        try {
            const user = await auth.getUser()
            let company = await user.company().fetch()
            company = company.first()
            let roles = await Role.query().where('company_id',company.id).fetch()
            response.status(200).json({ roles })
        } catch (error) {
            return response.status(401).json({ message: 'Falha ao Listar', original: error.message })
        }
    }
}

module.exports = RoleController
