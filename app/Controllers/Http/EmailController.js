'use strict'
const Mail = use('Mail')

class EmailController {

    async boleto ({ request, response }) {
        try {
            await Mail.send('emails.boleto', {}, (message) => {
                message.subject('Boleto')
                message.from('dafirsouza@gmail.com')
                message.to('joao.victor@smartnx.io')
            })
            response.status(202).json({ message: 'Enviado com sucesso' })
        } catch (error) {
            response.status(401).json({ message: error.message, originalMessage: error.message })
        }
    }
}

module.exports = EmailController
