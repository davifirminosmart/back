'use strict'
const TimeLine = use('App/Models/TimeLine')
class TimeLineController {

  async index ({ request, response, auth }) {
    try {
      const { id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const timeline = await TimeLine
        .query()
        .select('users.username', 'timelines.protocol', 'timelines.created_at', 'timelines.title', 'timelines.description')
        .innerJoin('users', 'users.id', 'timelines.user_id')
        .where('id_conductor', '=', id)
        .where('company_id', company.id)
        .orderBy('timelines.created_at', 'desc')
        .fetch()

      response.status(200).json({ timeline })
      } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
}

module.exports = TimeLineController
