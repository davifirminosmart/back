'use strict'
const User = use('App/Models/User')

class AdminSessionController {
  async create ({ request, auth, response }) {
    try {
      const { email, password } = request.all()
      const user = await User.findBy('email', email)
      if(!user) throw new Error('Usuário não encontrado')
      const roles = await user.getRoles()
      if(roles[0] ==! 'smart') throw new Error('Você não tem permissão para acessar a página')
      const token = await auth.attempt(email, password)
      return response.status(200).json({ token })
    } catch (error) {
      return response.status(401).json({ message: error.message, original: error.message })
    }
  }
}

module.exports = AdminSessionController
