'use strict'
const Conductor = use('App/Models/Conductor')
const Timeline = use('App/Models/TimeLine')
const HelpDesk = use('App/Models/HelpDesk')

class CreditCardController {

  async adicional({request, response, auth}) {
    try {
      const { idClienteDependente } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let cartaoTitular = await Conductor.getAdicional(idClienteDependente, company.id)
      console.log(cartaoTitular)
      // console.log(`cartão ${cartaoTitular[0].id} dependente ${idClienteDependente}`)
      let respCliente = await Conductor.getClientById({id: idClienteDependente }, company.id)
      console.log(respCliente)
      let dataClient = respCliente.status === 400 ? null : respCliente
      let bloqueado = 0
      let cards = null
      
      if(dataClient && cartaoTitular[0]) {
      if(cartaoTitular[0].bloqueioCartao) bloqueado = 1
      
      let respfatura = await Conductor.faturaFechada(idClienteDependente, company.id)
      let lancamentos = respfatura.status === 400 ? null : respfatura.lancamentos
      let { valorTotalFaturaAtual } = await Conductor.faturaAtual(idClienteDependente, company.id)
      let { saldoDisponivel, limiteSaque } = await Conductor.informacoesCliente(idClienteDependente, company.id)
      let ultimoExtrato =  null || lancamentos.reduce( (acumulador, current) => current.valor + acumulador, 0)
      let anuidade = await Conductor.getAnuidade(idClienteDependente, company.id)
      if(dataClient) {
        let numeroCartao = cartaoTitular[0].panMascarado.split('X')
        cards = {
          bandeira: cartaoTitular[0].bandeira,
          primeiroQuarto:numeroCartao[0],
          segundoQuarto: numeroCartao[numeroCartao.length - 1],
          limite: dataClient.extratoClienteResponse.limite,
          saldoDisponivel: dataClient.extratoClienteResponse.saldoDisponivel,
          id: cartaoTitular[0].id,
          diaVencimento: dataClient.ciclo.diaVencimento,
          melhorDia: dataClient.melhorDiaCompra,
          minimo: dataClient.faturaFechada.valorPagamentoMinimo,
          ultimoPagamento: dataClient.faturaFechada.somaPagamentos,
          saldoUltimoExtrato: Number(ultimoExtrato).toFixed(2),
          ultimoExtratoEnviado: dataClient.dataFechamentoProxFatura,
          comprasPeriodo: valorTotalFaturaAtual,
          limiteAvista: saldoDisponivel,
          limiteSaque: limiteSaque,
          idResponsavel: idClienteDependente,
          anuidade: anuidade.valor,
          bloqueado,
          nome: cartaoTitular[0].nomePortadorCartao,
          obsBloqueio: bloqueado === 1 ? cartaoTitular[0].bloqueioCartao.observacao : ''
        }
      }
    }
      return response.status(200).json({ cards  })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async bloquear({request, response, auth}) {
    try {
      const { id, cpf, protocolo, id_conductor, tipo, observacao } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      let resp = await Conductor.blockCard(id, observacao, tipo, company.id)
      if(resp.status === 400) throw new Error(resp.detail)
      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: cpf,
          protocol: protocolo,
          id_conductor,
          user_id: user.id, 
          title: 'Cartão Bloqueado', 
          description: `O usuário: ${user.username}, bloqueou o cartão com a seguinte observação: ${observacao}.`,
          card: 1,
          card_id: id,
          company_id: company.id
        }
      )
      await timeline.save()
      response.status(200).json({ message: 'Bloqueado com sucesso' })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async desbloquear({request, response, auth}) {
    try {
      const { id, cpf, protocolo, id_conductor, observacao } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      let resp = await Conductor.desbloquear(id, observacao, company.id)
      if(resp.status === 400) throw new Error(resp.detail)
      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: cpf,
          protocol: protocolo,
          id_conductor,
          user_id: user.id, 
          title: 'Cartão Desbloqueado', 
          description: `O usuário: ${user.username}, desbloqueou o cartão com a seguinte observação: ${observacao}.`,
          card: 1,
          card_id: id,
          company_id: company.id
        }
      )
      await timeline.save()
      response.status(200).json({ message: 'Desbloqueado com sucesso' })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
  async cardHistory({ request, response, auth }) {
    try { 
      const { id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      const timeline = await Timeline
        .query()
        .select('timelines.*')
        .where('card_id', '=', id)
        .where('company_id', company.id)
        .orderBy('timelines.created_at', 'desc')
        .fetch()      
      return response.status(200).json({ timeline })
    } catch(error) {
      return response.status(401).json(
        { 
          message: error.message, 
          originalMessage: error.message 
        }
      )
    }
  }

  async novoAdicional({ request, response, auth }) {
    try {
      const { titular } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      const idPessoa = titular.idPessoa
      delete titular.idPessoa
      const novoContato = titular.telefones.map(tel => {
        delete tel.id
        return tel
      })
      
      const novoEndereco = titular.endereco.map(end => {
        delete end.id
        return end
      })
      
      company = company.first()
      let respCond = await Conductor.adicional({ ...titular, telefones: novoContato, endereco: novoEndereco[0] }, idPessoa, company.id)
      console.log('RESPOSTAAAAAA')
      console.log(respCond)
      if(respCond.status == 400) throw new Error(respCond.detail)

      console.log('Dependentes', respCond)
      
      response.status(200).json({ message: 'Criado com sucesso' , dependente: respCond })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async limite ({request, response, auth}) {
    try {
      const { card, cpf, name, id_conductor, protocolo, email, obs } = request.all()
      let idHelpDesk = ''
      let personType = cpf.length > 11 ? 2 : 1
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let clientHelpDesk = await HelpDesk.getClient({ cpfCnpj: cpf }, company.id)
      
      
      if(!clientHelpDesk.length) {
        clientHelpDesk = await HelpDesk.createClient({isActive: true, personType, profileType: 2, userName: name, businessName: name, corporateName: name, cpfCnpj: cpf, 
          emails: [{
            emailType: "Profissional",
            email,
            isDefault: true
          }]  
        }, company.id)
        idHelpDesk = clientHelpDesk.id
      } else {
        idHelpDesk = clientHelpDesk[0].id
      }
      
      await HelpDesk.createTicket(idHelpDesk, personType, obs, 'Alteração de limite', company.id)
      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: cpf,
          protocol: protocolo,
          id_conductor,
          user_id: user.id, 
          title: 'Alteração de limite', 
          description: `O usuário: ${user.username}, Solicitou a alteração de limite para o cartao com o final: ${card.segundoQuarto}.`,
          card: 1,
          card_id: card.id,
          company_id: company.id
        }
      )
      await timeline.save()
      
      response.status(200).json({ message: 'Ticket Criado com Sucesso' })
    } catch (error) {
      console.log(error.message)
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async solicitacao ({request, response, auth}) {
    try {
      const { card, cpf, name, id_conductor, protocolo, email, obs } = request.all()
      let idHelpDesk = ''
      let personType = cpf.length > 11 ? 2 : 1
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let clientHelpDesk = await HelpDesk.getClient({ cpfCnpj: cpf }, company.id)
      if(!clientHelpDesk.length) {
        clientHelpDesk = await HelpDesk.createClient({isActive: true, personType, profileType: 2, userName: name, businessName: name, corporateName: name, cpfCnpj: cpf, 
          emails: [{
            emailType: "Profissional",
            email,
            isDefault: true
          }]  
        }, company.id)
        idHelpDesk = clientHelpDesk.id
      } else {
        idHelpDesk = clientHelpDesk[0].id
      }
      
      await HelpDesk.createTicket(idHelpDesk, personType, obs, 'Solicitação Especial', company.id)
      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: cpf,
          protocol: protocolo,
          id_conductor,
          user_id: user.id, 
          title: 'Solicitação Especial', 
          description: `O usuário: ${user.username}, fez uma solicitação especial para o cartao com o final: ${card.segundoQuarto}.`,
          card: 1,
          card_id: card.id,
          company_id: company.id
        }
      )
      await timeline.save()
      
      response.status(200).json({ message: 'Ticket Criado com Sucesso' })
    } catch (error) {
      console.log(error.message)
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async contestacao ({request, response, auth}) {
    try {
      const { card, cpf, name, id_conductor, protocolo, email, obs } = request.all()
      let idHelpDesk = ''
      let personType = cpf.length > 11 ? 2 : 1
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      let clientHelpDesk = await HelpDesk.getClient({ cpfCnpj: cpf }, company.id)
    
      
      if(!clientHelpDesk.length) {
        clientHelpDesk = await HelpDesk.createClient({isActive: true, personType, profileType: 2, userName: name, businessName: name, corporateName: name, cpfCnpj: cpf, 
          emails: [{
            emailType: "Profissional",
            email,
            isDefault: true
          }]  
        }, company.id)
        idHelpDesk = clientHelpDesk.id
      } else {
        idHelpDesk = clientHelpDesk[0].id
      }
      
      await HelpDesk.createTicket(idHelpDesk, personType, obs, 'Contestação', company.id)
      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: cpf,
          protocol: protocolo,
          id_conductor,
          user_id: user.id, 
          title: 'Contestação', 
          description: `O usuário: ${user.username}, fez uma contestação para o cartao com o final: ${card.segundoQuarto}.`,
          card: 1,
          card_id: card.id,
          company_id: company.id
        }
      )
      await timeline.save()
      
      response.status(200).json({ message: 'Ticket Criado com Sucesso' })
    } catch (error) {
      console.log(error.message)
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async sms ({ request, response, auth }) {
    try {
      const { id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let respCond = await Conductor.smsCard(id, company.id)
      console.log(respCond)
      response.status(200).json({ message: 'Enviado com Sucesso' })
      } catch (error) {
        response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
}

module.exports = CreditCardController
