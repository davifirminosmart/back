'use strict'
const Ticket = use('App/Models/Ticket')
const Timeline = use('App/Models/TimeLine')

class TicketController {

  async create ({ request, response, auth }) {
    try {
      const { client } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const ticket = new Ticket()
      const timeline = new Timeline()
      const TITLE = `Novo Atendimento`
      const DESCRIPTION = 'Novo atendimento ticket criado'

      await ticket.fill({ client_cpf: client.cpf, user_id: user.id, id_conductor: client.id, company_id: company.id })
      await ticket.save()

      await timeline.fill({ client_cpf: client.cpf, user_id: user.id, id_conductor: client.id, protocol: ticket.protocol, title: TITLE, description: DESCRIPTION, company_id: company.id })
      await timeline.save()

      response.status(200).json({ ticket })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

}

module.exports = TicketController
