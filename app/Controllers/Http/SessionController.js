'use strict'
const User = use('App/Models/User')
const Conductor = use('App/Models/Conductor')

class SessionController {
  async create ({ request, auth, response }) {
    try {
      const { email, password } = request.all()
      const user = await User.findBy('email', email)
      if(!user) throw new Error('Usuário não encontrado')
      
      let company = await user.company().fetch()
      company = company.first()
      const roles = await user.getRoles()
      console.log('empresa', company)
      if(roles[0] === 'smart') throw new Error('Você não tem permissão para acessar a página')

      const token = await auth.attempt(email, password)
      const permissions = await user.getPermissions()

      const roleName = await user.getNameRole(user.id) 

      let tiposBloqueio = await Conductor.tiposBloqueio(company.id)
      
      return response.status(200).json({ token, user, permissions, role: roleName, tiposBloqueio })
    } catch (error) {
      return response.status(401).json({ message: 'Usuário ou senha não encontrado', original: error.message })
    }
  }

}

module.exports = SessionController
