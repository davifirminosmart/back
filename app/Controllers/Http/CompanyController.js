'use strict'
const User = use('App/Models/User')
const Company = use('App/Models/Company')
const Role = use('Adonis/Acl/Role')

class CompanyController {
  async create({ request, response, auth }) {
    try {
      const { user, company } = request.all()
      const companyInstance = new Company()
      const userInstance = new User()
      const suporteInstance = new User()
      const adminRole = new Role()
      const roleConsultivo = new Role()
      const roleBasico = new Role()
      const roleAvancado = new Role()

      // Cria Empresa
      await companyInstance.fill(company)
      await companyInstance.save()

      // Cria Role Admin
      await adminRole.fill({name: 'Admin', slug: 'admin', description: 'Usuário Admin', company_id: companyInstance.id })
      await adminRole.save()
      
      // Cria Usuario Admin
      await userInstance.fill(user)
      await userInstance.save()
      await userInstance.roles().attach([adminRole.id])
      
      // Cria Usuário Suporte
      await suporteInstance.fill({
        username: 'Suporte Smart',
        email: `suporte-smart@${company.name.normalize('NFD').replace(/[\u0300-\u036f]/g, '').split(' ').join('').toLowerCase()}.com`,
        birthday: '12/12/2012',
        password: 'EY!(SJ.G'
      })
      await suporteInstance.save()
      suporteInstance.roles().attach([2])

      // Vincula Usuario a empresa
      await companyInstance.users().attach([userInstance.id, suporteInstance.id])

      // Cria Roles Restantes
      await roleConsultivo.fill({name: 'Consultivo', slug: 'consultivo', description: 'Usuário Consultivo', company_id: companyInstance.id })
      await roleConsultivo.save()
      await roleBasico.fill({name: 'Basico', slug: 'basico', description: 'Usuário Básico', company_id: companyInstance.id })
      await roleBasico.save()
      await roleAvancado.fill({name: 'Avançado', slug: 'avancado', description: 'Usuário Avançado', company_id: companyInstance.id })
      await roleAvancado.save()

      // Adiciona as permissoes as roles
      roleConsultivo.permissions().attach([4, 5, 6,10, 12, 16])
      adminRole.permissions().attach([4,5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ,20, 21])
      roleBasico.permissions().attach([4,5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17])
      roleAvancado.permissions().attach([4,5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21])
      
      return response.status(200).json({ message: 'Criado com Sucesso' })
    } catch (error) {
      return response.status(401).json({ message: 'Falha ao criar', original: error.message })
    }
  }
}

module.exports = CompanyController
