'use strict'
const HelpDesk = use('App/Models/HelpDesk')
const Conductor = use('App/Models/Conductor')
class ClientController {

  async index({request, response, auth }) {
    try {
      let { cpfCnpj, hasCpf } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      
      let client = []
      let card = []
      let dependentes = []
      let meses = ['','janeiro', 'feveirero', 'março', 'abril', 'maio','junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro']
      let prepareDate = ''
      let bloqueado = 0
      if(Number(hasCpf)) prepareDate = await Conductor.getClient({ cpf: cpfCnpj }, company.id)
      if(!Number(hasCpf)) prepareDate = await Conductor.getCompany({ cnpj: cpfCnpj }, company.id)
      console.log('respconductor', prepareDate)
      let clientes = prepareDate.clientes
      if(!clientes[0]) throw new Error('Cliente Não encontrado')
      if(clientes) {
        let dataClient = await Conductor.getClientById({id: clientes[0].idCliente}, company.id)
        let cartaoTitular = await Conductor.consultarCartaoTitular(clientes[0].idCliente, company.id)
        if(cartaoTitular[0].bloqueioCartao) bloqueado = 1
        
        let { lancamentos } = await Conductor.faturaFechada(clientes[0].idCliente, company.id)
        let { valorTotalFaturaAtual } = await Conductor.faturaAtual(clientes[0].idCliente, company.id)
        let { saldoDisponivel, limiteSaque } = await Conductor.informacoesCliente(clientes[0].idCliente, company.id)
        let anuidade = await Conductor.getAnuidade(clientes[0].idCliente, company.id)
        
        
        let ultimoExtrato =  lancamentos.reduce( (acumulador, current) => current.valor + acumulador, 0)
        if(dataClient) {
          let numeroCartao = cartaoTitular[0].panMascarado.split('X')
          client = [{
            name: dataClient.nome,
            id: dataClient.id,
            cpf: dataClient.cpf,
            email: dataClient.email,
            rg: dataClient.rg,
            address_residencial: dataClient.enderecoResidencial,
            address_comercial: dataClient.enderecoComercial,
            address_outro: dataClient.enderecoAlternativo,
            mesNascimento: meses[Number(dataClient.clienteDataNascimento.split('-')[1])],
            anoNascimento: dataClient.clienteDataNascimento.split('-')[0]
          }]
          card = [{
            bandeira: cartaoTitular[0].bandeira,
            primeiroQuarto:numeroCartao[0],
            segundoQuarto: numeroCartao[numeroCartao.length - 1],
            limite: Number(dataClient.extratoClienteResponse.limite).toFixed(2),
            saldoDisponivel: Number(dataClient.extratoClienteResponse.saldoDisponivel).toFixed(2),
            id: cartaoTitular[0].id,
            diaVencimento: dataClient.ciclo.diaVencimento,
            melhorDia: dataClient.melhorDiaCompra,
            minimo: Number(dataClient.faturaFechada.valorPagamentoMinimo).toFixed(2),
            ultimoPagamento: Number(dataClient.faturaFechada.somaPagamentos).toFixed(2),
            saldoUltimoExtrato: Number(ultimoExtrato).toFixed(2),
            ultimoExtratoEnviado: dataClient.dataFechamentoProxFatura,
            comprasPeriodo: Number(valorTotalFaturaAtual).toFixed(2),
            limiteAvista: Number(saldoDisponivel).toFixed(2),
            limiteSaque: Number(limiteSaque).toFixed(2),
            idResponsavel: dataClient.id,
            anuidade: Number(anuidade.valor).toFixed(2),
            nome: dataClient.nome,
            bloqueado,
            obsBloqueio: bloqueado === 1 ? cartaoTitular[0].bloqueioCartao.observacao : ''

          }]
          dependentes = dataClient.dependentes
        }
      
      }

      response.status(200).json({ client, card, dependentes })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async update({request, response, auth}) {
    try {
      let { client, id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let resp = await Conductor.updateClient(client, id, company.id)
      response.status(200).json({ message: 'Atualizado com sucesso' })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async contacts({ request, response, auth }) {
    try {
      const { id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const contacts = await Conductor.getClientContact(id, company.id)
      response.status(200).json({ contacts })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async address({ request, response, auth }) {
    try {
      const { id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const address = await Conductor.informacoesCliente(id, company.id)
      response.status(200).json({ address })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }

  async cancelarAnuidade ({ request, response, auth }) {
    try {
      const { id } = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let respCond = await Conductor.cancelarAnuidade(id, company.id)
      if(respCond.status == 400) throw new Error(respCond.detail)
      response.status(200).json({ message: 'cancelado com sucesso' })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
  async contactsCreate({ request, response, auth }) {
    try {
      const {ddd, id, numero, tipoContato} = request.all()
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      let respCond = await Conductor.novoContato({ ddd,numero,tipoContato }, company.id, id)
      console.log(respCond)
      if(respCond.status == 400) throw new Error(respCond.detail)
      response.status(200).json({ message: 'Criado com sucesso' , data: respCond })
    } catch (error) {
      response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
}
module.exports = ClientController
