'use strict'
const Conductor = use('App/Models/Conductor')
const Infobip = use('App/Models/Infobip')
const Moment = require('moment')
const Env = use('Env')
const LOGIN_INFOBIP = Env.get('LOGIN_INFOBIP')
const PASSWORD_INFOBIP = Env.get('PASSWORD_INFOBIP')
const Mail = use('Mail')
const Timeline = use('App/Models/TimeLine')

class SendController {
  
  async sendEmail({ request, response, auth }) {
    try {
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const { client_id, invoice_id, email, protocol, 
        clientData } = request.all()
    
      let bank_account = await Conductor.getBankAccount(company.id)
      const bank_id = bank_account[0].id
      let cardData = await Conductor.listInvoiceDetailed(client_id, invoice_id, company.id)

      const nameCard = cardData.nomeCartao
      const aux = cardData.nomeCartao.split(' ')
      const firstName = aux[0]
      const expiryDate = cardData.dataVencimento = Moment(cardData.dataVencimento).format('DD/MM/YYYY')
      const totalValue = cardData.valorTotalFaturaAtual
      const minimumValue = cardData.valorPagamentoMinimoFaturaAtual
      let barCodeData = await Conductor.getBarcodes(invoice_id, bank_id, company.id)
      if(barCodeData.status && barCodeData.status.toString().substr(0, 1) === '4') throw new Error('Falha ao gerar Boleto')
      
      const linhaDigitavel = barCodeData.linhaDigitavel
      
      const respEmail = await Mail.send('emails.boleto', {
        firstName,
        nameCard,
        expiryDate,
        totalValue,
        minimumValue,
        linhaDigitavel
      }, (message) => {
        message.subject('Fatura')
        message.from('dafirsouza@gmail.com')
        message.to(email)
      })

      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: clientData.cpf,
          protocol,
          id_conductor: client_id,
          user_id: user.id, 
          title: 'E-mail Enviado', 
          description: `O usuário: ${user.username}, enviou E-mail para: ${email}.`,
          company_id: company.id
        }
      )
      await timeline.save()

      return response.status(200).json({ 
        message: 'SMS enviado com sucesso', data: respEmail })
    } catch(error) {
      return response.status(401).json({ 
        message: error.message, originalMessage: error.message })
    }
  }

  async sendSMS({ request, response, auth }) {
    try {
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const { client_id, invoice_id, telephone, protocol, clientData } = request.all()
      const authorizationInfoBip = `${LOGIN_INFOBIP}:${PASSWORD_INFOBIP}`
      let buff = new Buffer(authorizationInfoBip)
      let base64 = buff.toString('base64')
      
      let bank_account = await Conductor.getBankAccount(company.id)
      const bank_id = bank_account[0].id
      let cardData = await Conductor.listInvoiceDetailed(client_id, invoice_id, company.id)
      const nameCard = cardData.nomeCartao
      const numberCard = cardData.numeroCartaoTruncado.substr(-4)
      const expiryDate = cardData.dataVencimento = Moment(cardData.dataVencimento).format('DD/MM/YYYY')
      const totalValue = cardData.valorTotalFaturaAtual
      let barCodeData = await Conductor.getBarcodes(invoice_id, bank_id, company.id)
      if(barCodeData.status && barCodeData.status.toString().substr(0, 1) === '4') throw new Error('Falha ao gerar Boleto')
      const barCode = barCodeData.codigoBarras
      const message = `Fat.${nameCard} final ${numberCard} vcto ${expiryDate} Total: RS ${totalValue} Cod.Barras:${barCode}`
      
      let respInfoBip = await Infobip.sendSMS(telephone, message, base64)

      const timeline = new Timeline()
      await timeline.fill(
        { 
          client_cpf: clientData.cpf,
          protocol,
          id_conductor: client_id,
          user_id: user.id, 
          title: 'SMS Enviado', 
          description: `O usuário: ${user.username}, enviou SMS para o telefone: ${telephone}.`,
          company_id: company.id
        }
      )
      await timeline.save()

      return response.status(200).json({ 
        message: 'SMS enviado com sucesso', data: respInfoBip })
    } catch(error) {
      return response.status(401).json({ 
        message: error.message, originalMessage: error.message })
    }
  }

}

module.exports = SendController
