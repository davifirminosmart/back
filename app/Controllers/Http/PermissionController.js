'use strict'
const Permission = use('Adonis/Acl/Permission')
const Database = use('Database')
const Role = use('Adonis/Acl/Role')

class PermissionController {
    async index ({ request, response }) {
        try {
            let permissions = await Permission.all()
            let groups = ['TimeLine', 'Perfil', 'Cartão', 'Fatura']
            response.status(200).json({ permissions, groups })
        } catch (error) {
            response.status(401).json({ message: error.message, originalMessage: error.message })
        }
    }

    async list ({ request, response, params }) {
        try {
            const id = params.id
            const permissionsQuery = await Database
            .select('permissions.id')
            .table('permissions')
            .innerJoin('permission_role', 'permission_role.permission_id', 'permissions.id')
            .where('permission_role.role_id', id)
            
            const permissions = permissionsQuery.map(_p => _p.id)
            response.status(200).json({ permissions })
        } catch (error) {
            response.status(401).json({ message: error.message, originalMessage: error.message })
        }
    }

    async update ({ request, response }) {
        try {
            const { id, permissions } = request.all()
            const role = await Role.find(id)
            await role.permissions().detach()
            await role.permissions().attach(permissions)
            response.status(200).json({ message: 'atualizado com sucesso'})
        } catch (error) {
            response.status(401).json({ message: error.message, originalMessage: error.message })
        }
    }
}

module.exports = PermissionController
