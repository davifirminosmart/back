'use strict'

const Conductor = use('App/Models/Conductor')
const Moment = require('moment')

class LastPurchaseController {
  async list({ params, response, auth }) {
    try { 
      const user = await auth.getUser()
      let company = await user.company().fetch()
      company = company.first()
      const today = Moment().startOf('month').format('YYYY-MM-DD')
      const lastMonth = Moment().endOf('month').format('YYYY-MM-DD') 
      let resp = await Conductor.listInvoiceByPeriod(params.id, today, lastMonth, company.id)
      const faturas = resp.faturasResponse.map(resp => {
        resp.dataVencimento = Moment(resp.dataVencimento).format('DD/MM/YYYY')
        return resp
      })
      return response.status(200).json(
        { 
          message: 'Listado com sucesso', 
          data: faturas
        }
      )
    } catch(error) {
      return response.status(401).json({ message: error.message, originalMessage: error.message })
    }
  }
}

module.exports = LastPurchaseController
