'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('/users', 'UserController.create')
Route.put('/users', 'UserController.update')
Route.get('/users', 'UserController.index')
Route.post('/login', 'SessionController.create')
Route.get('/client', 'ClientController.index').middleware('auth')
Route.put('/client', 'ClientController.update').middleware('auth')
Route.post('/cancelarAnuidade', 'ClientController.cancelarAnuidade').middleware('auth')
Route.post('/ticket', 'TicketController.create').middleware('auth')
Route.get('/timeline', 'TimeLineController.index').middleware('auth')
Route.get('/contatos', 'ClientController.contacts').middleware('auth')
Route.post('/contatos', 'ClientController.contactsCreate').middleware('auth')
Route.get('/enderecos', 'ClientController.address').middleware('auth')
Route.get('/adicionais', 'CreditCardController.adicional').middleware('auth')
Route.post('/senhasms', 'CreditCardController.sms').middleware('auth')
Route.post('/adicional', 'CreditCardController.novoAdicional').middleware('auth')
Route.post('/bloquear', 'CreditCardController.bloquear').middleware(['auth:jwt', 'can:block_card'])
Route.post('/desbloquear', 'CreditCardController.desbloquear').middleware(['auth:jwt', 'can:unblock_card'])
Route.get('/cardHistory', 'CreditCardController.cardHistory').middleware('auth')
Route.get('/invoices', 'InvoiceController.list').middleware(['auth'])
Route.get('/lastPurchases/:id', 'LastPurchaseController.list').middleware(['auth'])
Route.get('/detailedInvoice', 'InvoiceController.detailed').middleware(['auth'])
Route.post('/sendEmail', 'SendController.sendEmail').middleware(['auth'])
Route.post('/sendSMS', 'SendController.sendSMS').middleware(['auth'])
Route.post('/enviarBoleto', 'EmailController.boleto')
Route.get('/permissions', 'PermissionController.index').middleware(['auth'])
Route.put('/permissions', 'PermissionController.update').middleware(['auth'])
Route.get('/permissions/:id', 'PermissionController.list').middleware(['auth'])
Route.get('/roles', 'RoleController.list').middleware(['auth'])
Route.post('/limite', 'CreditCardController.limite').middleware(['auth'])
Route.post('/solicitacao', 'CreditCardController.solicitacao').middleware(['auth'])
Route.post('/contestacao', 'CreditCardController.contestacao').middleware(['auth'])

// Rotas Admin
Route.group(() => {
  Route.post('/login', 'AdminSessionController.create')
  Route.post('/empresa', 'CompanyController.create').middleware(['auth:jwt', 'can:create_company'])
}).prefix('admin')
